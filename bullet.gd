extends Area2D

@export var speed: int = 5500
@export var secondsToLive: int = 500 # TODO clear bullet after x seconds

# Called when the node enters the scene tree for the first time.
func _ready():
	# Aim the bullet at where the mouse is the instant of its creation
	look_at(get_global_mouse_position())
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	position += transform.x * speed * delta # move along the x axis
	pass

# Called when colliding with another object (body)
func _on_body_entered(body):
	if body.is_in_group("mobs"):
		body.queue_free()
		# defer the call to delete the bullet, reduce load with many objects
		# TODO switch to object pool system for bullets
	call_deferred("queue_free")
