extends CharacterBody2D

var bullet: PackedScene = preload("res://scenes/bullet.tscn")

const speed: int = 50
# ammunition
const mag_size: int = 7
var cur_mag: int = mag_size
var reserve_ammo: int = 50

# signals
signal gained_score(change: int)

# get the sprite handle
@onready var sprite: Sprite2D = %Sprite

func _ready():
	# set ammo counts
	$magazine.text = str(mag_size)
	$reserve_ammo.text = str(reserve_ammo)

func _process(_delta):
	# check for reload
	if Input.is_action_pressed("reload"): reload()
	
	# test signal emitting
	if Input.is_action_pressed("tester"): gained_score.emit(1) # TODO Remove Me!
	

func _physics_process(_delta):
	velocity = get_movement_on_axis()
	#print(velocity)
	look_at(get_global_mouse_position())
	move_and_slide()
	
	# shoot
	if Input.is_action_just_pressed("shoot"): shoot()



# determines the current character movement
# returns a V2 for velocity
func get_movement_on_axis() -> Vector2:
	var v: Vector2
	
	var left_right: float = Input.get_axis("move_left", "move_right")
	v.x = left_right
	
	var up_down: float = Input.get_axis("move_up", "move_down")
	v.y = up_down
	
	v = v.normalized() * speed
	
	return v
	

func shoot():
	if can_shoot():
		update_mag(cur_mag - 1)
		var b = bullet.instantiate()
		owner.add_child(b)
		b.transform = $Sprite/Muzzle.global_transform

func can_shoot() -> bool:
	if cur_mag > 0:
		return true
	return false

func reload():
	# only count bullets missing from the mag
	var requested_bullet_count = mag_size - cur_mag
	# if we do not have enough ammo to replenish, reload only that much
	var bullets_to_reload: int = min(reserve_ammo, requested_bullet_count)
	reserve_ammo -= bullets_to_reload
	update_mag(cur_mag + bullets_to_reload)
	update_reserve_ammo(reserve_ammo)

# update_magazine state updates both the internal magazine variable and the
# player-visible magazine label
func update_mag(m: int):
	assert(m >= 0, "mag cannot be set less than 0")
	assert(m <= mag_size, "mag cannot be set greater than mag_size(%s)" % mag_size)
	cur_mag = m
	$magazine.text = str(m)

# update_reserve_ammo updates both the internal magazine variable and the
# player-visible magazine label
# a is the amount to set reserve_ammo to
func update_reserve_ammo(a: int):
	assert(a >= 0, "reserve ammo cannot be set less than 0")
	reserve_ammo = a
	$reserve_ammo.text = str(a)
