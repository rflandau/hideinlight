extends CanvasLayer

var kills_signal

# Called when the node enters the scene tree for the first time.
func _ready():
	# connect to external signals
	# TODO
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func set_kills(k: int):
	$kills.text = str(k)

# set_magazine updates the visual text for the mag ammo count
func set_magazine(m: int): 
	$magazine.text = str(m)
	pass
